#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void isEmptyPrimitiveTrue(){
    assert( ( Stream<int>::empty().isEmpty() ) );
    std::cout << "[TEST]::[isEmptyPrimitiveTrue]::SUCCESS" << std::endl;
}

void isEmptyPrimitiveFalse(){
    assert( ( !Stream<char>::of( { 'a' } ).isEmpty() ) );
    std::cout << "[TEST]::[isEmptyPrimitiveFalse]::SUCCESS" << std::endl;
}

void isEmptyAbstractTrue(){
    class Test{ public: int const x = 10; };
    assert( ( Stream<Test>::empty().isEmpty() ) );
    std::cout << "[TEST]::[isEmptyAbstractTrue]::SUCCESS" << std::endl;
}

void isEmptyAbstractFalse(){
    class Test{ public: int const x = 10; };
    assert( ( !Stream<Test>::of( { Test() } ).isEmpty() ) );
    std::cout << "[TEST]::[isEmptyAbstractTrue]::SUCCESS" << std::endl;
}