#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void limitPrimitiveEmpty(){
    auto stream = Stream<int>::empty().limit( 2 );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    assert( stream.toArray().length == 0 );
    std::cout << "[TEST]::[limitPrimitiveEmpty]::SUCCESS" << std::endl;
}

void limitPrimitive(){
    auto stream = Stream<int>::of( { 1, 2, 3, 4, 5 }).limit( 2 );
    assert( stream.size() == 2 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0] == 1 );
    assert( stream.toArray().array[1] == 2 );
    assert( stream.toArray().length == 2 );
    std::cout << "[TEST]::[limitPrimitive]::SUCCESS" << std::endl;
}

void limitiAbstractEmpty(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::empty().limit( 2 );
    assert( stream.size() == 0 );
    assert( stream.isEmpty() );
    assert( stream.toArray().length == 0 );
    std::cout << "[TEST]::[limitiAbstractEmpty]::SUCCESS" << std::endl;
}

void limitAbstract(){
    class Test{ public: int x = 10; };
    auto stream = Stream<Test>::of( { Test(), Test(), Test(), }).limit( 1 );
    assert( stream.size() == 1 );
    assert( !stream.isEmpty() );
    assert( stream.toArray().array[0].x = 10 );
    assert( stream.toArray().length == 1 );
    std::cout << "[TEST]::[limitAbstract]::SUCCESS" << std::endl;
}