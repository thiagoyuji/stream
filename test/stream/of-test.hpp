#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void ofPrimitiveArray(){
    assert( ( Stream<int>::of( { 1 } ).size() == 1 ) );
    assert( ( Stream<float>::of( { float(1.1), float(1.1) } ).size() == 2 ) );
    assert( ( Stream<char>::of( { 'a', 'b', 'c' } ).size() == 3 ) );
    assert( ( Stream<double>::of( { 1.1, 1.1, 1.1, 1.1 } ).size() == 4 ) );
    assert( ( Stream<std::string>::of( { std::string("ola") } ).size() == 1 ) );
    assert( ( Stream<bool>::of( { true, false, true, false, true, false } ).size() == 6 ) );
    std::cout << "[TEST]::[ofPrimitiveArray]::SUCCESS" << std::endl;    
}

void ofAbstractArray(){
    class Test{ public: int const x = 10; };
    assert( ( Stream<Test>::of( { Test() } ).size() == 1 ) );
    std::cout << "[TEST]::[ofAbstractArray]::SUCCESS" << std::endl;
}