#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void forEachPrimitiveConsumerNull(){
    try{
        Stream<int>::of( { 10 } ).forEach( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[forEachPrimitiveConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void forEachPrimitiveConsumer(){
    int sum = 0;
    Stream<int>::of( { 10, 10, 10, 10, 10 } ).forEach( [ &sum ]( int const value ){ sum += value; } );
    assert( sum == 50 );
    std::cout << "[TEST]::[forEachPrimitiveConsumer]::SUCCESS" << std::endl;
}

void forEachAbstractConsumerNull(){
    class Test{ public: int const x = 10; };
    try{
        Stream<Test>::of( { Test() } ).forEach( nullptr );
        assert( false );
    }catch( const std::invalid_argument & exception ){
        assert( true );
        std::cout << "[TEST]::[forEachAbstractConsumerNull]::SUCCESS::" << exception.what() << std::endl;
    }
}

void forEachAbstractConsumer(){
    int sum = 0;
    class Test{ public: int const x = 10; };
    Stream<Test>::of( { Test(), Test() } ).forEach( [ &sum ]( Test const value ){ sum += value.x; } );
    assert( sum == 20 );
    std::cout << "[TEST]::[forEachAbstractConsumer]::SUCCESS" << std::endl;
}

void forEachPrimitiveEmpty(){
    int sum = 0;
    Stream<int>::empty().forEach( [ &sum ]( int const value ){ sum += value; } );
    assert( sum == 0 );
    std::cout << "[TEST]::[forEachPrimitiveEmpty]::SUCCESS" << std::endl;
}

void forEachAbstractEmpty(){
    int sum = 0;
    class Test{ public: int const x = 10; };
    Stream<Test>::empty().forEach( [ &sum ]( Test const value ){ sum += value.x; } );
    assert( sum == 0 );
    std::cout << "[TEST]::[forEachAbstractEmpty]::SUCCESS" << std::endl;
}