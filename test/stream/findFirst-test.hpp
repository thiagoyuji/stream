#include <iostream>
#include <cassert>

#include "../../include/stream/stream.hpp"

void findFirstPrimitiveEmpty(){
    assert( Stream<int>::empty().findFirst().get() == 0 );
    std::cout << "[TEST]::[findFirstPrimitiveEmpty]::SUCCESS" << std::endl;
}

void findFirstPrimitive(){
    assert( Stream<int>::of( { 10, 20, 30 } ).findFirst().get() == 10 );
    std::cout << "[TEST]::[findFirstPrimitive]::SUCCESS" << std::endl;
}

void findFirstAbstractEmpty(){
    class Test{ public: int x = 10; };
    assert( Stream<Test>::empty().findFirst().get().x == 10 );
    std::cout << "[TEST]::[findFirstAbstractEmpty]::SUCCESS" << std::endl;
}

void findFirstAbstract(){
    class Test{ public: int x = 10; };
    Test test = Test();
    test.x = 20;
    assert( Stream<Test>::of( { Test(), test } ).findFirst().get().x == 10 );
    std::cout << "[TEST]::[findFirstAbstract]::SUCCESS" << std::endl;
}