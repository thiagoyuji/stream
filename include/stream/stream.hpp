#pragma once

#include <stdexcept>
#include <array>

#include "../lambda/lambda.hpp"
#include "../optional/optional.hpp"

template <typename T> struct Array {

	T * array;

	size_t const length;

	Array( T * array, size_t const length ) : array( array ), length( length ){}

	~Array(){ delete[] array; }

};

template <class T> class Stream {

    public:

        ~Stream(){
            if( delete_t ) delete[] t;
        }

        Stream( T const * const t, size_t const length, bool const delete_t ) : t( t ), length( length ), delete_t( delete_t ){}

        template <size_t _length> static Stream<T> of( T const (&t)[ _length ] ){
            return Stream<T>( t, _length );
        }

        static Stream<T> empty(){ return Stream<T>( {}, 0 ); }

        size_t const size() const { return length; }

        bool isEmpty() const { return size() == 0; }

        bool allMatch( Predicate<T const> const predicate ) const {

            if( predicate == nullptr ) throw std::invalid_argument("Stream::allMatch - Predicate cannot be NULL!!");

            if( isEmpty() ) return false;

            bool match = true;

            for( int count = 0; count < length; count++ ){

                if( !predicate( t[count] ) ) match = false;

            }

            return match;

        }

        bool anyMatch( Predicate<T const> const predicate ) const {
            
            if( predicate == nullptr ) throw std::invalid_argument("Stream::anyMatch - Predicate cannot be NULL!!");
            
            bool match = false;
            
            for( int count = 0; count < length; count++ ){
                
                if( predicate( t[ count ] ) ) match = true;
                
            } 
            
            return match;
            
        }

        void forEach( Consumer<T const> const consumer ){
            
            if( consumer == nullptr ) throw std::invalid_argument("Stream::forEach - Consumer cannot be NULL !!");
            
            for( int count = 0; count < length; count++ ){
                
                consumer( t[ count ] );
                
            }

        }

        Stream<T> filter( Predicate<T const> const predicate ){

            if( predicate == nullptr ) throw std::invalid_argument("Stream::filter - Predicate cannot be NULL!!");

            T * const new_t = new T[ length ];
            
            size_t new_size = 0;

            for( int count = 0; count < length; count++ ){
                
                if( predicate( t[ count ] ) ) {
                    
                    new_t[ new_size++ ] = t[ count ];
                    
                }
                
            }
            
            return Stream<T>( new_t, new_size, true );
            
        }

        template <class S> Stream<S> map( Mapper<T const , S const> const mapper ){
            
            if( mapper == nullptr ) throw std::invalid_argument("Stream::map - Mapper cannot be NULL!!");
            
            S * const new_t = new S[ length ];
            
            size_t new_size = 0;
            
            for( int count = 0; count < length; count++ ){
                
                S const mapped_value = mapper( t[ count ] );
                
                new_t[ new_size++ ] = mapped_value;
                
            }
            
            return Stream<S>( new_t, new_size, true );

        }

        Optional<T> reduce( BinaryOperation<T const> const binaryOperation ){

            if( binaryOperation == nullptr ) throw std::invalid_argument("Stream::reduce - BinaryOperation cannot be NULL!!");

            if( isEmpty() ) return Optional<T>::of( T() );

            T sum = T();

            for( int count = 0; count < length; count++ ){

                sum = binaryOperation( t[ count ], sum );

            }

            return Optional<T>::of( sum );

        }

        Stream<T> limit( size_t const max_size ){

            if( isEmpty() ) return Stream<T>::empty();

            T * const new_t = new T[ max_size ];

            size_t new_size = 0;

            for( int count = 0; count < max_size; count++ ){
                
                new_t[ new_size++ ] = t[ count ];
                
            }
            
            return Stream<T>( new_t, max_size, true );

        }

        Optional<T> findFirst(){

            if( isEmpty() ) return Optional<T>::of( T() );

            return Optional<T>::of( t[ 0 ] );

        }

        Array<T> toArray(){ 

            Array<T> mutable_array = Array<T>( new T[ length ], length ); 

            for( int count = 0; count < length; count++ ){

                mutable_array.array[ count ] = t[ count ];

            }

            return mutable_array;

        }

    private:
        
        T const * const t;

        size_t const length;

        bool const delete_t = false;

        Stream( T const * const t, size_t const length ) : t( t ), length( length ){}

};

#include "stream-pointer.hpp"