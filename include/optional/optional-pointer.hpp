#include "optional.hpp"

template <class T> class Optional <T *> {

	public:

		~Optional(){ delete t; }

		static Optional<T *> of( T const * const t ){ 
			if( t == nullptr ) throw std::invalid_argument("[OPTIONAL::of] - Value cannot be NULL!!");
			return Optional<T *>( t ); 
		}

		static Optional<T *> ofNullable( T const * const t ){ return Optional<T *>( t ); }

		static Optional<T *> empty(){ return Optional<T *>( nullptr ); }

		T const get() const { 
			if( isEmpty() ) throw nullptr_value("[OPTIONAL::get] - Cannot return NULL value!!");
			return *t; 
		}

		T const * const getPointer() const {
			if( isEmpty() ) throw nullptr_value("[OPTIONAL::get] - Cannot return NULL value!!");
			return t; 
		}

		bool isEmpty() const { return t == nullptr; }

		bool isPresent() const { return !isEmpty(); }

		T const orElse( T const other ){
			if( !isPresent() ) return other;
			return *t;
		}

		T const * const orElse( T const * const other ){
			if( other == nullptr ) throw std::invalid_argument("[OPTIONAL::orElse] - Value cannot be NULL!!");
			if( !isPresent() ) return other;
			return t;
		}

		T const orElseGet( Provider<T const> const provider ){
			if( provider == nullptr ) throw std::invalid_argument("[OPTIONAL::orElseGet] - Provider cannot be NULL!!");
			if( isPresent() ) return *t;
			return provider();
		}

		T const * const orElseGetPointer( Provider<T const * const> const provider ){
			if( provider == nullptr ) throw std::invalid_argument("[OPTIONAL::orElseGetPointer] - Provider cannot be NULL!!");
			if( isPresent() ) return t;
			return provider();
		}

		template <class E> T const getOrElseThrow( Provider<E const> const provider ){
			if( provider == nullptr ) throw std::invalid_argument("[OPTIONAL::getOrElseThrow] - Provider cannot be NULL!!");
			if( !isPresent() ) throw provider();
			return *t;
		}

		template <class E> T const * const getPointerOrElseThrow( Provider<E const> const provider ){
			if( provider == nullptr ) throw std::invalid_argument("[OPTIONAL::getPointerOrElseThrow] - Provider cannot be NULL!!");
			if( !isPresent() ) throw provider();
			return t;
		}

		Optional<T *> filter( Predicate<T const * const> const predicate ){
			if( predicate == nullptr ) throw std::invalid_argument("[OPTIONAL::filter] - Predicate cannot be NULL!!");
			if( isPresent() and predicate( t ) ) return Optional<T *>::of( new T( *t ) );
			return Optional<T *>::empty();
		}

		template <class E> Optional<E> map( Mapper<T const * const, E const> const mapper ){
			if( mapper == nullptr ) throw std::invalid_argument("[OPTIONAL::map] - Mapper cannot be NULL!!");
			if( isPresent() ) return Optional<E>::of( mapper( t ) );
			return Optional<E>::ofNullable( E() );
		}

		void ifPresent( Consumer<T const * const> const consumer ){
			if( consumer == nullptr ) throw std::invalid_argument("[OPTIONAL::ifPresent] - Consumer cannot be NULL!!");
			if( isPresent() ) consumer( t );
		}

		void ifNotPresent( Action const action ){
			if( action == nullptr ) throw std::invalid_argument("[OPTIONAL::ifNotPresent] - Action cannot be NULL!!");
			if( !isPresent() ) action();
		}

	private:

		T const * const t;

		Optional( T const * const t ) : t( t ){}

};
