# **Stream - C++** 

<img alt="Snowflask" src="stream.png" width="100">

Stream is library that encapsulate an array and has operations on it. This Operatios allow to control array and it's elements.

## **Sumary**

- [About Me](#about-me)
- #### **Static methods**
- [Of](#of-method)
- [Empty](#empty-method)
- #### **Instance methods**
- [Size](#size-method)
- [IsEmpty](#isempty-method)
- [AllMatch](#allmatch-method)
- [AnyMatch](#anymatch-method)
- [ForEach](#foreach-method)
- [Filter](#filter-method)
- [Map](#map-method)
- [Reduce](#reduce-method)
- [Limit](#limit-method)
- [FindFirst](#findfirst-method)
- [ToArray](#toarray-method)

---

## **`Stream static methods`** 

---

1. #### *of method*

```cpp
    template <size_t _length> static Stream<T> of( T const (&t)[ _length ] );
```
- *`Stream::of`* is a method that encapsulate an input array.

Input                             | Output
----------------------------------|--------------------------
T const (&t)[ _length ]           | Stream< T >

---

2. #### *empty method*

```cpp
    static Stream<T> empty();
```
- *`Stream::empty`* is a method that encapsulate an empty array.

Input   | Output
--------|--------------------------
\-      | Stream< T >

---

## **`Stream instance methods`** 

---

1. #### *size method*

```cpp
    size_t const size();
```
- *`Stream.size`* is a method that return the size of the encapsulated array.

Input | Output
------|-------
\-    | size_t

---

2. #### *isEmpty method*

```cpp
    bool isEmpty();
```
- *`Stream.isEmpty`* is a method that return true if size of the encapsulated array is 0, otherwise return false.

Input | Output
------|-------
\-    | bool

---

3. #### *allMatch method*

```cpp
    template <class E> using Predicate = std::function< bool ( E ) >;
```
- *`Predicate`* is a lambda function that enter E and return boolean.

```cpp
    bool allMatch( Predicate<T const> const predicate );
```
- *`Stream.allMatch`* is a method that return true if all elements of the encapsulated array match with predicate, otherwise return false. Predicate cannot be null, if it's, will throw an exception(std::invalid_argument).

Input             | Output
------------------|-------
Predicate< T >    | bool

---

4. #### *anyMatch method*

```cpp
    bool anyMatch( Predicate<T const> const predicate );
```
- *`Stream.anyMatch`* is a method that return true if unless one element of the encapsulated array match with predicate, otherwise return false. Predicate cannot be null, if it's, will throw an exception(std::invalid_argument).

Input             | Output
------------------|-------
Predicate< T >    | bool

---

5. #### *forEach method*

```cpp
    template <class R> using Consumer = std::function< void ( R ) >;
```
- *`Consumer`* is a lambda function that enter R and return nothing.

```cpp
    void forEach( Consumer<T const> const consumer );
```
- *`Stream.forEach`* is a method that execute some action in all elements of the encapsulated array. Consumer cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|-------
Consumer< T >    | void

---

6. #### *filter method*

```cpp
    Stream<T> filter( Predicate<T const> const predicate );
```
- *`Stream.filter`* is a method that test predicate in all elements of the encapsulated array, if test true add to new stream, otherwise do nothing. Predicate cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|-------------
Predicate< T >   | Stream< T >

---

7. #### *map method*

```cpp
    template <class T, class S> using Mapper = std::function< S ( T ) >;
```
- *`Mapper`* is a lambda function that enter T and return S. Map T in S.

```cpp
    template <class S> Stream<S> map( Mapper<T const , S const> const mapper );
```
- *`Stream.map`* is a method that execute mapper in all elements of the encapsulated array, Turning T in S. Mapper cannot be null, if it's, will throw an exception(std::invalid_argument).

Input            | Output
-----------------|--------------
Mapper< T, S >   | Stream< S >

---

8. #### *reduce method*

```cpp
    template <class T> using BinaryOperation = std::function< T ( T, T ) >;
```
- *`BinaryOperation`* is a lambda function that enter (T, T) first T is value, second T is accumulator; and return T accumulated value.

```cpp
    Optional<T> reduce( BinaryOperation<T const> const binaryOperation )
```
- *`Stream.reduce`* is a method that execute a binary operation in all elements of the encapsulated array, accumulating it in the accumulator, at the end return accumulated value in Optional. BinaryOperation cannot be null, if it's, will throw an exception(std::invalid_argument).

Input                 | Output
----------------------|----------------
BinaryOperation< T >  | Optional< T >

---

9. #### *limit method*

```cpp
    Stream<T> limit( size_t const max_size );
```
- *`Stream.limit`* is a method that truncate the stream in max size and return new stream.

Input    | Output
---------|-------------
size_t   | Stream< T >

---

10. #### *findFirst method*

```cpp
    Optional<T> findFirst();
```
- *`Stream.findFirst`* is a method that return the first element from the encapsulated array in Optional.

Input | Output
------|-------------
\-    | Optional< T >

---

11. #### *toArray method*

```cpp
    Array<T> toArray();
```
- *`Stream.toArray`* is a method that return encapsulated array as type Array defined in stream. This array is mutable.

Input | Output
------|-------------
\-    | Array< T >

---

### **`About Me`**

> **Nick** : *STNIN*

> **Author** : *Thiago Hayaci*

> **Email** : *tyhayaci@gmail.com*

> **gitlab** : *https://gitlab.com/STNIN*

> **licence** : *free*
